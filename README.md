# Knowledge Transfer

Bachelor Thesis: Knowledge Distillation for fast Image Segmentation


### design document for ntrain.py with different KD methods:

user must be able to pick one KD method via args
user can pass KD options via args

if for example user chose RCO:
a logic at the beginning configures all necessary parameters similarly to load_datatset
the RCO method will want to:

configure RCO:
    previous teacher = empty model
    previous student = empty student

    teacher_callbacks
    student_callbacks
    custom_objects_teacher
    custom_objects_student
    metrics_teacher
    metrics_student
    loss func teacher
    loss func student

perform RCO:
    load previous teacher
    train 50 steps
    save teacher output
    (save teacher in file)

    load teacher dataset
    load previous student
    train 50 steps
    (save student in file)

some things always stay the same:
the callbacks and metrics for all teachers/students
the architecture of teachers and students respectively won't change over execution of ntrain.py
the ability to initialize the first teacher and first student from file is always required
the ability to initialize the first dataset for teacher and student will always be required
the student performance (on original dataset) shall be evaluated

question: should wandb callbacks and checkpoints be seperated for teacher vs student?
run dir:
- teacher/
    section1models/
      model1
      model2
      model3
    section2models/
      model1
- student/
    section1/
      model1
      model2